#! /bin/bash
# Create virtual enviroment if specified by user
CURRENT_PATH=/home/mpiuser/docker
USER_REQS="https://gitlab.com/project-dare/dare-api/raw/master/examples/wp6/requirements/cr_dl_jsn_reqs.txt"
USERNAME="Th1s4sY0urT0k3Nn"
RUN_DIR="run"
set -e
cd ..
mkdir -p sfs
cd sfs
mkdir -p d4p
cd d4p
mkdir -p $RUN_DIR
cd $RUN_DIR
if [ ! $USER_REQS == "None" ]
then
    # Creating virtual enviroment
    python3 -m venv --system-site-packages `echo $USERNAME`_env
    source `echo $USERNAME`_env/bin/activate
    pip install cwlref-runner
    # Split string with delimeter "/" and get last split argument
    wget ${USER_REQS}
    pip install --upgrade pip setuptools==45
    pip install -I -r ${USER_REQS##*/}
    deactivate
    # mpi requirements sharing
    ln -s /home/mpiuser/docker/dispel4py .
    cd dispel4py
    export PATH="/home/mpiuser/sfs/d4p/${RUN_DIR}/${USERNAME}_env/bin/"
    python -c "import sys; print(sys.executable)"
    python setup.py install
    source /etc/profile
    cd ..
    unlink dispel4py
fi


cwlVersion: v1.0
class: CommandLineTool
baseCommand: [mpiexec]

inputs:
- id: no_processes
  type: int
  inputBinding:
    prefix: -n
    position: 1

- id: run_id
  type: string
  inputBinding:
    prefix: -x
    position: 2

- id: run_path
  type: string?
  inputBinding:
    prefix: -x
    position: 3

- id: d4py
  type: string
  inputBinding:
    position: 4

- id: target
  type: string
  inputBinding:
    position: 5

- id: workflow 
  type: string
  inputBinding:
    position: 6

- id: attribute
  type: Any?
  inputBinding:
    prefix: -a
    position: 7

- id: inputfile
  type: string? 
  inputBinding:
    prefix: -f
    position: 8

- id: inputdata
  type: Any?
  inputBinding:
    prefix: -d
    position: 9

- id: iterations
  type: int?
  inputBinding:
    prefix: -i
    position: 10

outputs: []

import os
import helper_functions as hf
import requests

# Constant hostnames of exec-api and d4p-registry api
LOGIN_HOSTNAME = "https://testbed.project-dare.eu/dare-login"
D4P_REGISTRY_HOSTNAME = 'https://testbed.project-dare.eu/d4p-registry'
# D4P_REGISTRY_HOSTNAME = "http://localhost:8000"
# PLAYGROUND_API_HOSTNAME = 'https://testbed.project-dare.eu/playground'
PLAYGROUND_API_HOSTNAME = "http://localhost:5000"
upload_folder = "d4p-input"
upload_file = "input.json"
zipped_upload_file = "input.zip"
exec_command = "pip install --user matplotlib"
run_dir = "sthemeli_Run1_2305"
dowload_filename = "logs.txt"


def test_debug_d4p(token):
    try:
        hf.delete_workspace('mySplitWorkspace', D4P_REGISTRY_HOSTNAME, token)
    except (BaseException, Exception):
        pass

    # Register a workspace
    workspace_url, workspace_id = hf.create_workspace("", "mySplitWorkspace", "", D4P_REGISTRY_HOSTNAME, token)
    workspace_id = int(workspace_id)
    print('Workspace URL: ' + D4P_REGISTRY_HOSTNAME + '/workspaces/' + str(workspace_id))
    print('Workspace ID: ' + str(workspace_id))

    # Register ProcessingElementSignature
    pe_url = hf.create_pe(desc="", name="mySplitMerge", conn=[], pckg="mysplitmerge_pckg", workspace=workspace_url,
                          clone="", peimpls=[], hostname=D4P_REGISTRY_HOSTNAME, token=token)
    print('PESig URL: ' + D4P_REGISTRY_HOSTNAME + '/pes/' + str(pe_url.split('/')[4]))

    # Online code
    code = requests.get(
        'https://gitlab.com/project-dare/exec-api/-/raw/master/examples/mySplitMerge/scripts/mySplitMerge_prov.py')
    req = requests.get('https://gitlab.com/project-dare/exec-api/-/raw/master/examples/mySplitMerge/scripts/reqs.txt')

    impl_id = hf.create_peimpl(desc="", code=str(code.text), parent_sig=pe_url, pckg="mysplitmerge_pckg",
                               name="mySplitMerge", workspace=workspace_url, clone="", hostname=D4P_REGISTRY_HOSTNAME,
                               token=token)
    print('PE Implementation ID: ' + str(impl_id))

    hf.debug_d4p(PLAYGROUND_API_HOSTNAME, impl_id, "mysplitmerge_pckg", workspace_id, "mySplitMerge", token, reqs=req,
                 output_filename="output.txt")


def test_exec_command(token, command):
    hf.exec_command(hostname=PLAYGROUND_API_HOSTNAME, token=token, command=command)


def test_upload(token, folder, filename, zipfilename):
    os.system("zip -r {} {}".format(zipfilename, filename))
    hf.upload(token=token, path=folder, local_path=os.getcwd(), hostname=PLAYGROUND_API_HOSTNAME, filename=filename)


def test_delegation_token(token):
    url = PLAYGROUND_API_HOSTNAME + "/test-delegation-token"

    data = {
        "access_token": token
    }

    response = requests.post(url, data=data)
    print(response.status_code, response.text)


if __name__ == '__main__':
    # login
    credentials = hf.read_credentials()
    access_token = hf.login(LOGIN_HOSTNAME, credentials["username"], credentials["password"], credentials["issuer"])[
        "access_token"]
    print(access_token)
    test_delegation_token(token=access_token)
    # debug d4p
    # test_debug_d4p(access_token)

    # simulate terminal
    # test_exec_command(access_token, exec_command)

    # upload
    # test_upload(token=access_token, folder=upload_folder, filename=upload_file, zipfilename=zipped_upload_file)

    # download
    # hf.download(hostname=PLAYGROUND_API_HOSTNAME, token=access_token, run_dir=run_dir, filename=dowload_filename,
    #             local_path=os.getcwd())

    # list test folders
    # hf.list_playground_dirs(hostname=PLAYGROUND_API_HOSTNAME, token=access_token)

    # list files inside a folder
    # hf.list_output_files(hostname=PLAYGROUND_API_HOSTNAME, token=access_token, run_dir="sthemeli_Run1_2305")

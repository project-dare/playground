import logging

import requests

try:
    from reg_client import VerceRegClient
except (BaseException, Exception):
    from app.reg_client import VerceRegClient


def get_client():
    logging.basicConfig()
    logger = logging.getLogger('DJREG_CLIENT')
    logger.setLevel(logging.INFO)
    # use client class
    client = VerceRegClient()
    client.manager.logged_in = True
    logger.info(client.manager.get_auth_token())
    return client


def authenticate(user, pasw):
    # log in registry
    logging.basicConfig()
    logger = logging.getLogger('DJREG_CLIENT')
    logger.setLevel(logging.INFO)

    # use client class
    client = VerceRegClient()
    client.manager.login(user, pasw)
    logger.info(client.manager.get_auth_token())
    return client


def get_pe(pe_id, wid, pckg, name, token, client):
    # get PE signature by <name>
    # getting implementation code from PE submission, has to be rethought
    pe = client.manager.get_pe_spec(wid, pckg, name, token)
    peimpls = pe['peimpls']
    code = ''
    # get pe implementation with id: <id>
    for impl in peimpls:
        id_str = impl.rsplit('/', 2)[1]
        if int(id_str) == int(pe_id):
            r = requests.get(impl, params={"access_token": token})
            code = r.json().get('code')
    if code == '':
        raise ImplementationNotFound()
    return code


class ImplementationNotFound(Exception):
    pass

import json
import logging
import os
import subprocess
from os.path import join, exists

import requests
import yaml
from kubernetes import client, config

from reg_client import VerceRegClient
from reg_lib import ImplementationNotFound
from from_registry import get_client, get_pe

OUTPUT = "output"
UPLOADS = "uploads"
RUNS = "runs"
DEBUG = "debug"


# Return $HOSTNAME pod (current pod running this code)
def init_from_yaml(name_space):
    """
    Uses the configuration yaml files of Kubernetes to get the hostname of the pod where the Playground API is
    deployed.

    Args
        name_space (str): the namespace where the DARE platform components are deployed

    Returns
        str: the hostname of the current pod
    """
    # Rbac authorization for inter container kubectl api calls
    config.load_incluster_config()
    # Init client
    v1 = client.CoreV1Api()
    # Get pods in dare-api namespace
    ret = v1.list_namespaced_pod(name_space)
    # Find ngix-api pod
    for i in ret.items:
        if i.metadata.name == os.environ['HOSTNAME']:
            return i


def get_code_and_specs(data, base_dir, run_dir, g):
    """
    Uses the D4p Information Registry API to retrieve the code of a specific PE Implementation. It is used in order
    to simulate a d4p execution in the DARE platform. Creates a yaml specification file for the CWL execution.

    Args
        | data (dict): the data provided by the user's request
        | base_dir (str): the base directory (debug) for the testing environment
        | run_dir (str): the new execution directory

    Returns
        dict: the code found in the registry and the yaml specification file for the CWL execution
    """
    data = json.loads(data)
    _args = data['d4p_args']
    # Peimpl
    code = find_pe_impl(data)
    # MPI spec
    _code = "/home/mpiuser/sfs/" + g.username + "/" + base_dir + "/" + run_dir + "/code.py"
    spec = yaml.dump(mpi_input(_code, **_args))
    return {'code': code, 'mpi_spec': spec}


def find_pe_impl(data):
    """
    Uses the D4p Registry to find a registered PE implementation

    Args
        data (dict): the data provided by the user's request

    Returns
        str: the registered code
    """
    # Authenticate user / GET auth token
    verce_client = get_client()
    # Get ProcessingElement Implementation code
    code = get_pe(data['impl_id'],
                  data['wrkspce_id'],
                  data['pckg'],
                  data['name'],
                  data["access_token"],
                  verce_client)
    return code


def mpi_input(workflow, **kw):
    """
    Creates the yaml specification file for the CWL execution of the dispel4py.

    Args
        | workflow (str): the path to the workflow code
        | **kw (dict): additional arguments

    Returns
        dict: the yaml specification
    """
    output_data = {
        'd4py': 'dispel4py',
        'target': 'simple',
        'workflow': workflow,
    }
    for k in kw:
        output_data[k] = kw.get(k)
        if isinstance(output_data[k], dict):
            output_data[k] = json.dumps(output_data[k])
    return output_data


def issue_delegation_token(data, g):
    token = data["access_token"]
    issuer = g.issuer
    try:
        host = os.environ[
            "DARE_LOGIN_PUBLIC_SERVICE_HOST"] if "DARE_LOGIN_PUBLIC_SERVICE_HOST" in os.environ else "localhost"
        port = os.environ["DARE_LOGIN_PUBLIC_SERVICE_PORT"] if "DARE_LOGIN_PUBLIC_SERVICE_PORT" in os.environ else 80
        url = "http://{}:{}/delegation-token".format(host, port)
        response = requests.post(url, data=json.dumps({"access_token": token, "issuer": issuer}))
        return response.status_code, response.text
    except (ConnectionError, Exception) as e:
        print(e)
        return 500, "Could not issue delegation token"


def create_necessary_folders(properties, run_dir, username):
    """
    Initializes the user's folders in the Shared File System if they do not exist.

    Args
        properties (dict): properties for the base and home directories in the SFS
        run_dir (str): the new execution directory
        username (str): the user's permanent id
    """
    if not exists(join(properties['home_dir'], username)):
        os.mkdir(join(properties['home_dir'], username))
    if not exists(join(properties['home_dir'], username, properties['base_directory'])):
        os.mkdir(join(properties['home_dir'], username, properties['base_directory']))
    if not exists(join(properties['home_dir'], username, properties['base_directory'], run_dir)):
        os.mkdir(join(properties['home_dir'], username, properties['base_directory'], run_dir))
    if not exists(join(properties["home_dir"], username, properties["base_directory"], run_dir, OUTPUT)):
        os.mkdir(join(properties["home_dir"], username, properties["base_directory"], run_dir, OUTPUT))


def define_environ(properties, request_data, run_dir, run_id, delegation_token, g):
    """
    Creates environmental variables necessary for the dispel4py execution in the testing environment.

    Args
        properties (dict): properties for the base and home directories in the SFS
        request_data (dict): the data of the user's request to the platform
        run_dir (str): the new execution directory
        run_id (str): uuid generated for the new execution
    """
    os.environ["BASE_DIR"] = properties["base_directory"]
    os.environ["USER_REQS"] = request_data["reqs"]
    os.environ["USERNAME"] = g.username
    os.environ["USER_ID"] = g.user_id
    os.environ["ISSUER"] = g.issuer
    os.environ["RUN_DIR"] = run_dir
    os.environ["RUN_ID"] = run_id
    os.environ["DELEGATION_TOKEN"] = delegation_token

    data = {
        "impl_id": request_data['impl_id'],
        "pckg": request_data['pckg'],
        "wrkspce_id": request_data['wrkspce_id'],
        "name": request_data['name'],
        "n_nodes": 1,
        "access_token": request_data['access_token'],
        # "reqs": request_data['reqs'],
        "d4p_args": json.loads(request_data['d4p_args'])
    }

    os.environ['D4P_RUN_DATA'] = json.dumps(data)
    return data


def save_code_and_spec(properties, run_dir, specs, username):
    """
    Saves in the new execution directory the retrieved code from the registry and the yaml specification for
    the CWL execution.

    Args
        properties (dict): properties for the base and home directories in the SFS
        run_dir (str): the new execution directory
        specs (dict): the yaml specification
        username (str): the user's permanent id
    """
    with open(join(properties['home_dir'], username, properties['base_directory'], run_dir, 'code.py'),
              'w') as code_file:
        code_file.write(specs['code'])

    with open(join(properties['home_dir'], username, properties['base_directory'], run_dir, 'spec.yaml'),
              'w') as spec_file:
        spec_file.write(specs['mpi_spec'])


def run_subprocess(command, output_dir):
    """
    Executes a bash script/command from inside python code and returns the output result.
    Retrieves the Kubernetes logs and returns them to the user

    Args
        command (str): the bash command to be executed
        output_dir (str): the output directory inside the current execution dir where the logs will be saved.

    Returns
        str: the logs of the execution
    """
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    output, error = process.stdout, process.stderr
    logs = []
    if output:
        output_logs = output.readlines()
        for line in output_logs:
            line = line.decode("utf-8")
            logs.append(line)
        logs.append("=====================================")
    if error:
        error_logs = error.readlines()
        for line in error_logs:
            line = line.decode("utf-8")
            logs.append(line)
    with(open(join(output_dir, "logs.txt"), 'w')) as f:
        f.writelines(logs)
    process.wait()
    return logs


def get_workflow_output(output_file, format="txt"):
    """
    Opens the output file of the execution and returns its contents.

    Args
        output_file (str): the path to the output file

    Returns
        str: the content of the output file
    """
    if format == "txt":
        exec_output = None
        if exists(output_file):
            with(open(output_file, 'r')) as f:
                exec_output = f.readlines()
        return exec_output
    elif format == "json":
        exec_output = None
        if exists(output_file):
            with(open(output_file, 'r')) as f:
                exec_output = json.loads(f.read())
        return exec_output

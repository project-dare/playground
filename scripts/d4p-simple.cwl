cwlVersion: v1.0
class: CommandLineTool
baseCommand: [dispel4py]

inputs:
- id: target
  type: string
  inputBinding:
    position: 1

- id: workflow 
  type: string
  inputBinding:
    position: 2

- id: attribute
  type: Any?
  inputBinding:
    prefix: -a
    position: 3

- id: inputfile
  type: string? 
  inputBinding:
    prefix: -f
    position: 4

- id: inputdata
  type: Any?
  inputBinding:
    prefix: -d
    position: 5

- id: iterations
  type: int?
  inputBinding:
    prefix: -i
    position: 6

outputs: []

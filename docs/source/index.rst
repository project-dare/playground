.. Playground documentation master file, created by
   sphinx-quickstart on Wed Jan 29 23:43:41 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Playground's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   app
   utils

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
